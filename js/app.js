// En la siguiente línea, puede incluir prefijos de implementación que quiera probar.
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// No use "var indexedDB = ..." Si no está en una función.
// Por otra parte, puedes necesitar referencias a algun objeto window.IDB*:
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla nunca ha prefijado estos objetos, por lo tanto no necesitamos window.mozIDB*)


if (!window.indexedDB) {
    window.alert("Su navegador no soporta una versión estable de indexedDB. Tal y como las características no serán validas");
}

// var db;
// var request = indexedDB.open("MyTestDatabase");

// request.onerror = function(event) {
//     alert("Database error: " + event.target.errorCode);
// };
// request.onsuccess = function(event) {
//     //db = request.result;
// };

// Así se ve nuestra información de clientes.
const customerData = [
    { ssn: "444-44-4444", name: "Bill", age: 35, email: "bill@company.com" },
    { ssn: "555-55-5555", name: "Donna", age: 32, email: "donna@home.org" }
  ];

  const dbName = "the_name_2";

var request = indexedDB.open(dbName, 1);

request.onerror = function(event) {
    console.log("Database error: ",event.target.error);
  // Manejar errores.
};
request.onupgradeneeded = function(event) {
  var db = event.target.result;

  // Se crea un almacén para contener la información de nuestros cliente
  // Se usará "ssn" como clave ya que es garantizado que es única
  var objectStore = db.createObjectStore("customers", { keyPath: "ssn" });

  // Se crea un índice para buscar clientes por nombre. Se podrían tener duplicados
  // por lo que no se puede usar un índice único.
  objectStore.createIndex("name", "name", { unique: false });

  // Se crea un índice para buscar clientespor email. Se quiere asegurar que
  // no puedan haberdos clientes con el mismo email, asi que se usa un índice único.
  objectStore.createIndex("email", "email", { unique: true });

  // Se usa transaction.oncomplete para asegurarse que la creación del almacén
  // haya finalizado antes de añadir los datos en el.
  objectStore.transaction.oncomplete = function(event) {
    // Guarda los datos en el almacén recién creado.
    var customerObjectStore = db.transaction("customers", "readwrite").objectStore("customers");
    for (var i in customerData) {
      customerObjectStore.add(customerData[i]);
    }
  }
};

request = indexedDB.open(dbName, 2);

request.onupgradeneeded = function (event) {

    console.log("onupgradeneeded");

    var db = event.target.result;

    // Create another object store called "names" with the autoIncrement flag set as true.
    var objStore = db.createObjectStore("names", { autoIncrement : true });

    // Because the "names" object store has the key generator, the key for the name value is generated automatically.
    // The added records would be like:
    // key : 1 => value : "Bill"
    // key : 2 => value : "Donna"
    for (var i in customerData) {
        objStore.add(customerData[i].name);
    }
}