$token = '';

/**
 * 1.- LOGIN
 */

 var credenciales = {
    email: "oswaldo@inkwonders.com",
    password: "inkwonders"
};

let login = () => {
    return fetch('https://demo.booktrain.com.mx/api/login', {
        method: 'POST',
        body: JSON.stringify(credenciales),
        headers:{
          'Content-Type': 'application/json'
        }
    });
}

/**
 * 2.- ABRIR CAJA
 */

 var cajon = {
    billete_1000 : 0,
    billete_500 : 0,
    billete_200 : 1,
    billete_100 : 1,
    billete_50 : 2,
    billete_20 : 5,
    conteo_monedas: 5,
    monto_monedas: 13.5,
    observaciones: "Un billete de 100 está roto",
};

 let abrirCaja = () => {
    return fetch('https://demo.booktrain.com.mx/api/venta_movil/caja/abrir', {
        method: 'POST',
        body: JSON.stringify(cajon),
        headers:{
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
    });
}

/**
 * 3.- OBTENER LA CONFIGURAION DEL COLEGIO
 */
let configuracion = () => {
    return fetch('https://demo.booktrain.com.mx/api/venta_movil/configuracion', {
        method: 'GET',
        mode: 'cors',
        cache: 'default',
        headers:{
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
    });
}

const configurarColegio = async() => {

    resp = await login();
    resp = await resp.json();

    token = resp.data.token;

    resp = await configuracion();
    resp = await resp.json();

    if (resp.success == false) {
        // No se tenia caja abierta
        console.warn(resp.message)
        resp = await abrirCaja();
        resp = await res.json()
        console.log("abrirCaja", resp);
        resp = await configuracion();
        resp = await res.json()
        console.log("configuracion", resp);
    }

    console.log(resp);


}

configurarColegio();