import {Model} from '/dist/es2015/models/model.js';

class UserProfiles extends Model{
  static TableName = 'userProfile';
  
  user = () => {
    return this.hasOne(Users, 'id', 'userId')
  }
}

class Users extends Model {
  static TableName = 'users';
  
  userProfile = () => {
      // the third and fourth parameter are optional;
      // by default the function name would be used as parent models attribute.
      return this.hasOne(UserProfiles, 'userId', '_id', 'userProfile')
        .where('name', 'newName').with([...]).withCustom(['user']); 
  }
}

const settings = {
    name : 'the_name_2',
    version : 3, //version of database
    migrations : [{
        name : 'users', //name of table
        primary : 'id', //auto increment field (default id)
        ormClass: Users,
        columns : [{
            name : 'email', //other indexes in the database
            attributes : { //other keyPath configurations for the index
                unique : true
            }
        },{
          name : 'userProfiles', //name of table
          primary : 'id', //auto increment field (default id)
          ormClass: UserProfiles,
          columns : [{
              name : 'userId', //other indexes in the database
          },{
            name : 'userContacts',
            columns : [{
                name : 'phone',
                attributes : {
                    multiEntry: true
                }
            },{
                name : 'contactId', //name of the index
                index : 'medical.contactId' //if provided then indexing value will be this
            },{
                name : 'userId'
            }]
        }]
    }]
};